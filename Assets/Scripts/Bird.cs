using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Bird : MonoBehaviour
{
    public bool isStart;
    public float force;
    public SpriteRenderer birdSpriteRenderer;
    public Rigidbody2D birdRigidbody;
    public Text textScore;
    public GameObject panelGameOver;
    public Animator animator;

    public int m_Score;
    private bool m_isDie;
    private float m_tempDelayDie;
    private string Key_HighScore = "HighScore";


    // Start is called before the first frame update
    void Start()
    {
        isStart = false;
        m_isDie = false;
        animator.SetInteger("hp", 1);
        m_Score = 0;
        textScore.text = m_Score.ToString();
        panelGameOver.gameObject.SetActive(false);

        PlayerPrefs.SetInt(Key_HighScore, 10);
        PlayerPrefs.Save();

        //PlayerPrefs.GetInt("Key_HighScore", 100);

        int highScore = PlayerPrefs.GetInt(Key_HighScore, 100);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_isDie)
        {
            m_tempDelayDie -= Time.deltaTime;
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (!isStart)
            {
                isStart = true;
                birdRigidbody.simulated = true;
            }
            else
            {
                if (m_isDie)
                {
                    if (m_tempDelayDie <= 0)
                        Reloadgame();
                }
                else
                    birdRigidbody.velocity = new Vector3(0, force, 0);
            }


        }


    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log(collision.gameObject.name);
        if (collision.gameObject.tag == "Building")
        {
            if(m_Score > PlayerPrefs.GetInt(Key_HighScore))
            {
                PlayerPrefs.SetInt(Key_HighScore , m_Score);
                PlayerPrefs.Save();
            }
            //Destroy(gameObject);
            animator.SetInteger("hp", 0);
            m_isDie = true;
            m_tempDelayDie = 3;
            Debug.Log("You Lose");
            textScore.gameObject.SetActive(false);
            panelGameOver.gameObject.SetActive(true);

            //birdRigidbody.velocity = new Vector3(0, force, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("Trigger" + collision.gameObject.name);
        if (collision.gameObject.tag == "Item")
        {
            Destroy(collision.gameObject);
            m_Score++;
            textScore.text = m_Score.ToString();
            Debug.Log("Score " + m_Score);
            Debug.Log("Trigger " + collision.gameObject.name);
        }
    }

    public void Reloadgame()
    {
        SceneManager.LoadScene(0);
    }

}
