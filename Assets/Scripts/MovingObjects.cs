using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObjects : MonoBehaviour
{
    public float speed = 4;
    public Bird bird;
    public float m_time;
    // Start is called before the first frame update
    void Start()
    {
        RandomPosition();

    }

    // Update is called once per frame
    void Update()
    {
        if (!bird.isStart) return;
        m_time += Time.deltaTime;
        transform.position -= new Vector3((speed + m_time * 0.5f) * Time.deltaTime, 0, 0);
        if (transform.position.x < -12)
        {
            Vector3 m_CurrentPosition = transform.position;
            m_CurrentPosition.x = 11;
            transform.position = m_CurrentPosition;
            RandomPosition();
        }
    }

    private void RandomPosition()
    {
        Vector3 m_CurrentPosition = transform.position;
        m_CurrentPosition.y = Random.Range(-3.0f, 0.0f);
        transform.position = m_CurrentPosition;

    }
}
